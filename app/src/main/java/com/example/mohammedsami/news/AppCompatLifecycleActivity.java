package com.example.mohammedsami.news;

import android.arch.lifecycle.LifecycleRegistry;
import android.support.v7.app.AppCompatActivity;

public class AppCompatLifecycleActivity extends AppCompatActivity {

    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }
}
