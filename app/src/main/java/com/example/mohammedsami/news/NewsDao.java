package com.example.mohammedsami.news;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface NewsDao {
    @Query("select * from News")
    LiveData<List<News>> getNews();

    @Insert
    void addNew(News news);

    @Insert
    void addNews(List<News> news);

    @Delete
    void deleteNew(News news);

    @Query("DELETE FROM News")
    void deleteNews();
}
