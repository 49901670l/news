package com.example.mohammedsami.news;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.mohammedsami.news.databinding.FragmentDetailBinding;

public class DetailActivityFragment extends Fragment {

    private View view;

    private FragmentDetailBinding binding;


    public DetailActivityFragment() {
    }

    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent intent = getActivity().getIntent();

        if (intent != null) {
            News news = (News) intent.getSerializableExtra("news");
            if (news != null){
                updateUi(news);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<News>() {
            @Override
            public void onChanged(@Nullable News news) {
                updateUi(news);
            }
        });

        return view;
    }

    private void updateUi (News news) {
        Log.d("news", news.toString());

        binding.tvAutor.setText(news.getAuthor());
        binding.tvTitulo2.setText(news.getTitle());
        binding.tvDescripcion.setText(news.getDescription());

        WebSettings settings = binding.tvUrl.getSettings();
        settings.setJavaScriptEnabled(true);
        binding.tvUrl.setWebViewClient(new WebViewClient());
        binding.tvUrl.loadUrl(news.getUrl());

        if (news.getImage().equalsIgnoreCase("None") || news.getImage().equalsIgnoreCase("")){
            news.setImage("https://banner2.kisspng.com/20180402/qxq/kisspng-logo-newspaper-computer-icons-newspaper-5ac233d8726782.9067246315226766964686.jpg");
        }
        Glide.with(getContext()).load(
                news.getImage()
        ).into(binding.tvImageUrl);


    }
}
