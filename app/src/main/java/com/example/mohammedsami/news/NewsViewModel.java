package com.example.mohammedsami.news;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class NewsViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final NewsDao newsDao;
    private MutableLiveData<Boolean> loading;

    public NewsViewModel(@NonNull Application application) {
        super(application);
        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.newsDao = appDatabase.getNewsDao();
    }

    public LiveData <List<News>> getNews(){
        return newsDao.getNews();
    }

    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if (loading == null) {
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<News>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<News> doInBackground(Void... voids) {

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );

            String lengua = sharedPreferences.getString("language", "es");
            String tipusConulta = sharedPreferences.getString("consulta", "vista");

            API api = new API();
            ArrayList<News> result;

            if (tipusConulta.equalsIgnoreCase("vista")){
                result = api.getLanguage(lengua);
            }
            else {
                result = api.getLanguage(lengua);
            }

            Log.d("DEBUG", result != null ? result.toString() : null);

            newsDao.deleteNews();
            newsDao.addNews(result);
            return result;
        }


        @Override
        protected void onPostExecute(ArrayList<News> news) {
            super.onPostExecute(news);
            loading.setValue(false);
        }
    }
}
