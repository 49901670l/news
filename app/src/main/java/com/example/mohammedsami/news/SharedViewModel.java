package com.example.mohammedsami.news;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<News> selected = new MutableLiveData<News>();

    public void select (News news){
        selected.setValue(news);
    }

    public LiveData<News> getSelected (){
        return selected;
    }
}
