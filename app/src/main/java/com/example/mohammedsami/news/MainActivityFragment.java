package com.example.mohammedsami.news;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.mohammedsami.news.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private ArrayList<News> items;
    private NewsAdapter adapter;
    private NewsViewModel model;
    private ProgressDialog dialog;
    private SharedViewModel sharedModel;
    private FragmentMainBinding binding;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater);
        View view = binding.getRoot();

        ListView lvNews = (ListView) view.findViewById(R.id.lvNews);

        items = new ArrayList<>();
        adapter = new NewsAdapter(
                getContext(),
                R.layout.lv_news_row,
                items
        );

        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        binding.lvNews.setAdapter(adapter);

        binding.lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                News news = (News) adapterView.getItemAtPosition(position);
                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("news", news);
                    startActivity(intent);
                }
                else {
                    sharedModel.select(news);
                }
            }
        });

        model = ViewModelProviders.of(this).get(NewsViewModel.class);
        model.getNews().observe(this, new Observer<List<News>>() {
            @Override
            public void onChanged(@Nullable List<News> news) {
                adapter.clear();
                adapter.addAll(news);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("The data is loading, wait a moment please.");
        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if (mostrat) {
                    dialog.show();
                }
                else {
                    dialog.dismiss();
                }
            }
        });

        return view;
    }

    boolean esTablet () {
        return getResources().getBoolean(R.bool.tablet);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_news_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh){
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void refresh() {
        model.reload();
    }
}
