package com.example.mohammedsami.news;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtils {
    public static String get (String dataUrl) throws IOException {
        URL url = new URL(dataUrl);
        String response = null;

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.addRequestProperty("Authorization", "uWJslOY5GhlaNw9d0YIMmOhxDdOFsw6tie_2mXc7dEFPszWs");

        try {
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            response = readStream(inputStream);
        }catch (Exception e) {
            Log.e("Error", "Exception: " + e.getMessage());
        }
        finally {
            urlConnection.disconnect();
        }
        return response;
    }

    private static String readStream(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;
        StringBuilder response = new StringBuilder();

        while ((line = bufferedReader.readLine()) != null){
            response.append(line);
            response.append('\r');
        }
        bufferedReader.close();

        return response.toString();
    }
}
