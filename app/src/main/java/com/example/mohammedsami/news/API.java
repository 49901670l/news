package com.example.mohammedsami.news;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class API {
    private final String BASE_URL = "https://api.currentsapi.services/v1/latest-news";
    private final String BASE_URL2 = "https://api.currentsapi.services/v1/search";
    private final int PAGES = 5;

    ArrayList<News> getLanguage(String language) {
        return doCall("language", language);
    }

    private ArrayList<News> doCall(String consulta, String vista) {

        ArrayList<News> news = new ArrayList<>();
        for (int i = 0; i < PAGES; i++) {
            try {
                String url = getUrlPage(consulta, vista, i);
                String JsonResponse = HttpUtils.get(url);
                ArrayList<News> list = processJson(JsonResponse);
                news.addAll(list);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return news;
    }

    private String getUrlPage (String consulta, String vista, int paginas) {
        Uri builtUri = Uri.parse(BASE_URL2)
                .buildUpon()
                .appendQueryParameter(consulta, vista)
                .appendQueryParameter("page", String.valueOf(paginas))
                .build();
        return builtUri.toString();
    }

    private ArrayList<News> processJson(String jsonResponse) {
        ArrayList<News> news = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonNews = data.getJSONArray("news");
            for (int i = 0; i < jsonNews.length(); i++) {
                News news1 = new News();
                JSONObject jsonNew = jsonNews.getJSONObject(i);
                news1.setAuthor(jsonNew.getString("author"));
                news1.setDescription(jsonNew.getString("description"));

                if (jsonNew.has("image")){
                    news1.setImage(jsonNew.getString("image"));
                }
                else {
                    news1.setImage("");
                }

                news1.setLanguage(jsonNew.getString("language"));
                news1.setTitle(jsonNew.getString("title"));
                news1.setUrl(jsonNew.getString("url"));
                news.add(news1);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return news;
    }

}
