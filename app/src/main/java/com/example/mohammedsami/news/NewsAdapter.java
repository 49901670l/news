package com.example.mohammedsami.news;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mohammedsami.news.databinding.LvNewsRowBinding;

import java.util.List;

public class NewsAdapter extends ArrayAdapter<News> {

    public NewsAdapter(Context context, int resource, List<News> objects){
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        News news = getItem(position);
        Log.w("News: ", news.toString());

        LvNewsRowBinding binding = null;

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_news_row, parent, false);
        }
        else {
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.tvTitulo.setText(news.getTitle());

        return binding.getRoot();
    }
}
